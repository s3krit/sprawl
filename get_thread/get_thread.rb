require 'httpclient'
require 'json'
require 'pry'
class GetThread
  def initialize
    @lastthread = nil
    @board = "g"
    @keyword = /cyb/i
  end

  def self.find_latest_thread(keyword,board="g")
    threads = find_matching_threads(keyword,board="g")
    return 'No thread found' unless threads.size > 0
    sorted_threads = threads.sort_by{|hash| hash['no']}
    lastthread = sorted_threads.last
    "https://boards.4chan.org/#{board}/thread/#{lastthread['no']}"
  end

  def self.find_matching_threads(keyword,board="g")
    # Fetch the catalog and store each thread in an array
    board_url = "http://a.4cdn.org/#{board}/catalog.json"
    pages = JSON.parse(HTTPClient.get_content(board_url))
    threads = []
    pages.each do |page|
      page['threads'].each do |thread|
        if thread['sub'] and thread['sub'].downcase.include? keyword
          threads.push thread
        end
      end
    end
    threads
  end
end
