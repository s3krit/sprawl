require_relative './get_thread/get_thread.rb'
require 'pry'

class TopicStack
  include Cinch::Plugin

  def initialize(*)
    super
    @topicstack = []
    @seperator = " || "
  end

  def render_topic
    topic = ""
    @topicstack.each do |topic_element|
      if topic_element == '#THREAD#'
        topic_element = GetThread.find_latest_thread(bot.config.keyword)
      end
      topic += topic_element
      topic+=@seperator
    end
    topic.chomp @seperator
  end

  timer 5, method: :timed
  def timed
    new_topic = render_topic
    unless bot.channels.first.topic == new_topic or new_topic == ""
      bot.channels.first.topic = new_topic
    end
  end

  hook :post,  method: :update_topic
  def update_topic(m)
    m.channel.topic = render_topic
  end

  hook :pre, method: :check_privilege
  def check_privilege(m)
    unless m.channel.opped? m.user or m.channel.half_opped? m.user
      raise SecurityError.new "#{m.user.nick} not authorised"
    end
  end

  match /import$/, method: :import_topic
  def import_topic(m)
    @topicstack = m.channel.topic.split @seperator
    @topicstack.map! {|topic_element|
      if topic_element == "No thread found" or topic_element =~ /^https:\/\/boards.4chan.org\/g\/thread\/.*/
        topic_element = "#THREAD#"
      end
      topic_element
    }
  end

  match /shell$/, method: :drop_to_shell
  def drop_to_shell(m)
    binding.pry
  end

  match /push (.+)$/, method: :push_topic
  def push_topic(m, topic_element)
    @topicstack.push topic_element
  end

  match /pop$/, method: :pop_topic
  def pop_topic(m)
    @topicstack.pop
  end

  match /ins (\d+) (.+)$/, method: :ins_topic
  def ins_topic(m,pos,topic_element)
    @topicstack.insert pos.to_i, topic_element
  end

  match /del (\d+)/, method: :del_topic
  def del_topic(m,pos)
    @topicstack.delete_at pos.to_i
  end

end
