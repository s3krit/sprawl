require 'open-uri'
require 'cinch'
require 'nokogiri'
require 'httpclient'

class URLParse
  include Cinch::Plugin

  listen_to :channel

  def listen(m)
    urls = URI.extract(m.message)
    info = []
		if urls.size > 0
      url = urls.first
      # Fetch HEAD so we know what's coming
      head_resp = HTTPClient.head(url,follow_redirect: true)
      info.push head_resp.headers["Content-Type"].split(';').first
      info.push humanize(head_resp.headers["Content-Length"])
      # Don't bother fetching if it's more than 10MB
      if head_resp.headers["Content-Length"].to_i < (1048576 * 10)
        doc = Nokogiri::HTML(HTTPClient.get_content(url,follow_redirect: true))
        title = doc.xpath('//title').text
        info.insert(0,title) if title != ""
      end
		end
    m.channel.send(info.join(' | '))
  end

  def humanize(bytes)
    bytes = bytes.to_i
    friendly_bytes= "0"
    if bytes < 1024
      friendly_bytes = bytes.to_s+"B"
    end
    if bytes >= 1024 and bytes < 1048576
      friendly_bytes = ((bytes/1024.0).round(1)).to_s+"KB"
    end
    if bytes >= 1048576
      friendly_bytes = ((bytes/1048576.0).round(1)).to_s+"MB"
    end
    friendly_bytes
  end
end
    
