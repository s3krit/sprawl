require 'cinch'
require_relative './topic_stack.rb'
require_relative './url_parse.rb'
require_relative './urban_dictionary.rb'

bot = Cinch::Bot.new do
  configure do |c|
    c.server    = 'irc.rizon.net'
    c.port      = 6697
    c.ssl.use   = true
    c.nick      = "Sprawl"
    c.name      = "SprawlBot"
    c.channels  = ['#sprawlbot']
    c.keyword   = 'cyb'

    # Load the plugins
    c.plugins.plugins = [
      TopicStack,
      URLParse,
      UrbanDictionary
    ]
  end
end

bot.start
