require 'cinch'
require 'httpclient'
require 'json'
require 'open-uri'

class UrbanDictionary
  include Cinch::Plugin

  match /ud (.+)$/, method: :ud_find
  def ud_find(m,term)
    results = JSON.parse(HTTPClient.get_content(URI.escape("http://api.urbandictionary.com/v0/define?term=#{term}")))
    if results['result_type'] == 'no_results'
      message = "No results"
    else
      first_result = results['list'].first

      definition = first_result['definition']
      # Remove breaks
      definition.gsub!("\r\n"," ")
      # And concatenate to 200 characters
      definition = definition[0..200].gsub(/\s\w+\s*$/,"...")

      # Then we want to do the same with the example
      example = first_result['example']
      example.gsub!("\r\n"," ")
      example = example[0..200].gsub(/\s\w+\/*$/,"...")

      message = "[#{term}] #{definition} :: Example: #{example}"

      # Finally, make sure our message isn't longer than IRC max (512)
      message = message[0..509].gsub(/\s\w+\s*$/,"...")
    end
    m.channel.send message
  end
end
